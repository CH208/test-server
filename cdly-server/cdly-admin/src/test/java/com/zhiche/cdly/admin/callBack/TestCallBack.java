package com.zhiche.cdly.admin.callBack;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 11:34 2020/8/12
 */
public class TestCallBack {
    public static void main (String[] args) {
        Caller caller = new Caller();
        caller.setMyCallBack(new MyCallBack() {
            @Override
            public void func () {
                System.out.println("hello Word!");
            }
        });
        caller.doCall();
    }
}
