package com.zhiche.cdly.admin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 16:50 2020/7/23
 */
public class ListSort {
    public static void main (String[] args) {
        User user1 = new User();
        user1.setAge(19);
        user1.setName("cai");

        User user2 = new User();
        user2.setName("hua");
        user2.setAge(18);

        User user3 = new User();
        user3.setName("hua1");
        user3.setAge(20);

        List<User> sort2 = new ArrayList();
        sort2.add(user1);
        sort2.add(user2);
        sort2.add(user3);
        //Collections.sort(sort2);
        Collections.sort(sort2, new Comparator<User>() {
            @Override
            public int compare (User o1, User o2) {
                int diff = o1.getAge() - o2.getAge();
                if (diff > 0) {
                    return 1;
                } else if (diff < 0) {
                    return -1;
                }
                return 0;//相等为0
            }
        });

        //替代上面匿名实现的排序，lambda
        List<User> descByIdList =
                sort2.stream().sorted(Comparator.comparing(User::getAge)).collect(Collectors.toList());
        System.out.println(sort2.toString());
        System.out.println(descByIdList.toString());
        
    }
}
