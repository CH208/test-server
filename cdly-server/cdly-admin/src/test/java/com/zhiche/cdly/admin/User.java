package com.zhiche.cdly.admin;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 15:09 2020/7/23
 */
public class User implements Comparable<User> {
    private String name;
    private Integer age;

    public String getName () {
        return name;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }

    public void setName (String name) {
        this.name = name;
    }

    public Integer getAge () {
        return age;
    }

    public void setAge (Integer age) {
        this.age = age;
    }

    @Override
    public int compareTo (User o) {
        //升序
        // return this.age-o.getAge() ;
        //降序
        return o.getAge() - this.age;
    }
}
