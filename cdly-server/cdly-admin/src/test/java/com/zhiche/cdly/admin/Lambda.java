package com.zhiche.cdly.admin;

import java.util.Arrays;
import java.util.List;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:00 2020/7/23
 */
public class Lambda {
    public static void main (String[] args) {
        List<String> list = Arrays.asList("cat", "dog", "fox");
        String str = "I have a dog";
        //是否包含某个字符串
        System.out.println(list.stream().anyMatch(str::contains));

        //获取集合中最小的数字
    }
}
