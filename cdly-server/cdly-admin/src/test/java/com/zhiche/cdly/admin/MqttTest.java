package com.zhiche.cdly.admin;

import com.alibaba.fastjson.JSONObject;
import com.zhiche.cdly.WebApplication;
import com.zhiche.cdly.service.mqtt.PublishSubscribe;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:31 2019/8/28
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MqttTest {
    @Test
    public void test(){
        List<String> list = new ArrayList<>();
        list.add("b");
        String json = JSONObject.toJSONString(list);
        PublishSubscribe.publish(json);
        PublishSubscribe.subscribe();
    }
}
