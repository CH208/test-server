package com.zhiche.cdly.admin.callBack;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 11:32 2020/8/12
 */
public class Caller {
    private MyCallBack myCallBack;

    public void doCall () {
        myCallBack.func();
    }

    public void setMyCallBack (MyCallBack myCallBack) {
        this.myCallBack = myCallBack;
    }
}
