package com.zhiche.cdly.admin.controller.base;

import com.zhiche.cdly.service.base.ITestService;
import com.zhiche.cdly.supports.response.RestfulResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 16:47 2019/8/9
 */
@RestController
@Api(tags = "测试专用")
public class TestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private ITestService iTestService;

    @RequestMapping("/hello")
    @ApiOperation("查询测试接口queryTestData")
    @ApiImplicitParams({@ApiImplicitParam(name = "username",value = "用户名")})
    public RestfulResponse<String> queryTestData(){
        RestfulResponse<String> restfulResponse = new RestfulResponse<>(0,"success",null);
        iTestService.queryTestData();
        return restfulResponse;
    }

    /**
     * 获取文件内容
     * @return
     */
    @RequestMapping("/readFile")
    public RestfulResponse<String> readFile(){
        RestfulResponse<String> restfulResponse = new RestfulResponse<>(0,"success",null);
//        iTestService.readFile();
        return restfulResponse;
    }
}
