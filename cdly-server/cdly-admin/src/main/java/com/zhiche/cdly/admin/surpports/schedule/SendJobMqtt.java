package com.zhiche.cdly.admin.surpports.schedule;

import com.alibaba.fastjson.JSONObject;
import com.zhiche.cdly.service.mqtt.PublishSubscribe;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:03 2020/4/9
 */
@Component
public class SendJobMqtt {
    @Scheduled(cron = "0 0/2 * * * ?")
    public void publishData () {
        List<String> list = new ArrayList<>();
        list.add("a");
        String json = JSONObject.toJSONString(list);
        PublishSubscribe.publish(json);
        PublishSubscribe.subscribe();
    }
}
