package com.zhiche.cdly.utils;

/**
 * Created by beidouxing on 2018/6/7.
 */
public class DocPrefix {
    public static final String INBOUND_NOTICE = "IN";//入库通知单

    public static final String INBOUND_INSPECT = "II";//入库质检单

    public static final String INBOUND_PUTAWAY = "IP";//入库单

    public static final String OUTBOUND_NOTICE = "ON";//出库通知单

    public static final String OUTBOUND_PREPARE = "OP";//出库备料单

    public static final String OUTBOUND_SHIP = "OS";//出库单

    public static final String MOVEMENT = "MV"; //移位单

    public static final String STOCK_INIT = "SI";//期初库存单

    public static final String STOCK_ADJUST= "SA"; //库存调整单

}
