package com.zhiche.cdly.core.test;

import com.zhiche.cdly.utils.request.SnowFlakeId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author beidouxing
 * @create 2018-05-23
 **/
@RunWith(SpringJUnit4ClassRunner.class)
public class SnowFlakeIdTest {

    public Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void genIdTest() {
        SnowFlakeId snowFlakeId = new SnowFlakeId(0, 0);
        for (int i = 0; i < 10; i++) {
            logger.info("generated id is {}:", snowFlakeId.nextId());
        }
//		logger.info("generated id is {}:",snowFlakeId.nextId());
    }
}
