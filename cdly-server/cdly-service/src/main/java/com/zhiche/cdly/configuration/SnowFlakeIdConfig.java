package com.zhiche.cdly.configuration;

import com.zhiche.cdly.utils.request.SnowFlakeId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by beidouxing on 2018/5/30.
 */
@Configuration
@ConfigurationProperties(prefix = "snow.flake")
public class SnowFlakeIdConfig {
    @Value("${snow.flake.datacenterId}")
    private long datacenterId;

    @Value("${snow.flake.machineId}")
    private long machineId;

    @Bean
    public SnowFlakeId snowFlakeId(){
        return new SnowFlakeId(datacenterId,machineId);
    }

}
