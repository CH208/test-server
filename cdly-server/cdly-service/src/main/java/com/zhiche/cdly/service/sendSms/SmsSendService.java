package com.zhiche.cdly.service.sendSms;

import com.yunpian.sdk.model.Result;
import com.yunpian.sdk.model.SmsSingleSend;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:13 2018/11/13
 */
public interface SmsSendService {
    /**
     * 发送短信
     *
     * @param sendSmsParam
     * @return
     */
    Result<SmsSingleSend> sendSmsCode (SendSmsParam sendSmsParam);
}
