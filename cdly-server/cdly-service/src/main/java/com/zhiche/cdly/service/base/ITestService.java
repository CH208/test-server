package com.zhiche.cdly.service.base;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.cdly.domain.model.base.TestUser;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:16 2019/8/9
 */
public interface ITestService extends IService<TestUser> {
    void queryTestData ();
}
