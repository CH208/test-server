package com.zhiche.cdly.service.sendSms;

import java.io.Serializable;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:09 2018/11/13
 */
public class SendSmsParam implements Serializable {

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 询价单号
     */
    private String inquerySheetNo;

    public String getPhone () {
        return phone;
    }

    public void setPhone (String phone) {
        this.phone = phone;
    }

    public String getInquerySheetNo () {
        return inquerySheetNo;
    }

    public void setInquerySheetNo (String inquerySheetNo) {
        this.inquerySheetNo = inquerySheetNo;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("SendSmsParam{");
        sb.append("phone='").append(phone).append('\'');
        sb.append(", inquerySheetNo='").append(inquerySheetNo).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
