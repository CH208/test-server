package com.zhiche.cdly.service.sendSms;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.yunpian.sdk.YunpianClient;
import com.yunpian.sdk.model.Result;
import com.yunpian.sdk.model.SmsSingleSend;
import com.zhiche.lisa.core.supports.BaseException;
import com.zhiche.lisa.core.utils.HttpClientUtil;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:13 2018/11/13
 */
@Service
public class SmsSendServiceImpl implements SmsSendService {

    private static String APIKEY = "2ac15041e43d472237ff9ea879267a08";

    //竞价平台中标短信通知模板id
    private static String TPLID = "2613926";

    //编码方式
    private static String ENCODING = "UTF-8";

    //模板单条发送接口的http地址
    private static String URI_TPL_SEND_SMS = "https://sms.yunpian.com/v2/sms/tpl_single_send.json";

    //模板群发接口的http地址
    private static String URI_TPL_Batch_SEND_SMS = "https://sms.yunpian.com/v2/sms/tpl_batch_send.json";

    //连接超时时间
    private static int socketTimeout = 10000;

    /**
     * 单条发送验证码
     *
     * @param sendSmsParam 发送短信参数
     */
    @Override
    public Result<SmsSingleSend> sendSmsCode (SendSmsParam sendSmsParam) {
        Result<SmsSingleSend> result = new Result<>();
        try {
            //1、发送短信组装入参
            Map<String, String> param = Maps.newHashMap();
            param.put(YunpianClient.MOBILE, sendSmsParam.getPhone());
            param.put(YunpianClient.TPL_ID, TPLID);
            param.put(YunpianClient.APIKEY, APIKEY);
            //对短信内容进行转码
            String tpl_value = URLEncoder.encode("#inquirySheetNo#", ENCODING) + "="
                    + URLEncoder.encode(sendSmsParam.getInquerySheetNo(), ENCODING);
            param.put(YunpianClient.TPL_VALUE, tpl_value);

            //2、调用发送短信接口
            String strReturn = HttpClientUtil.post(URI_TPL_SEND_SMS, param, socketTimeout);

            //3、解析返回值
            SmsSingleSend smsSingleSend = JSON.parseObject(strReturn, SmsSingleSend.class);
            result.setMsg(smsSingleSend.getMsg());
            result.setCode(smsSingleSend.getCode());
            result.setDetail(smsSingleSend.getMsg());
            result.setData(smsSingleSend);
        } catch (UnsupportedEncodingException e) {
            throw new BaseException("短信发送编码转换异常");
        }
        return result;
    }
}
