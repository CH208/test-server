package com.zhiche.cdly.service.base;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.cdly.domain.model.base.AppVersion;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author beidouxing
 * @since 2018-07-17
 */
public interface IAppVersionService extends IService<AppVersion> {

    /**
     * 获取安卓版本更新
     */
    AppVersion getLastVersionAndroid ();

    /**
     * 获取ios版本更新
     */
    AppVersion getLastVersionIos ();

}
