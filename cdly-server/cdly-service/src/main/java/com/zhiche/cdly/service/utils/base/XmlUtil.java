package com.zhiche.cdly.service.utils.base;

import com.zhiche.cdly.supports.exception.BaseException;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;

public class XmlUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(XmlUtil.class);

    /**
     * 通过文件路径解析xml文件
     * @param path 文件路径
     */
    public static Document parseByFilePath(String path) {
        SAXReader reader = new SAXReader();
        Document document = null;
        try {
            document = reader.read(path);
        } catch (DocumentException e) {
            throw new BaseException(e.getMessage());
        }
        return document;
    }

    /**
     * 通过字符串解析xml文件
     */
    public static Document parseByString(String xml) throws Exception{
        SAXReader reader = new SAXReader();

        Document document = null;
        try {
            document = reader.read(new ByteArrayInputStream( xml.getBytes("utf-8")));
        } catch (Exception e) {
            throw e;
        }
        return document;
    }
}
