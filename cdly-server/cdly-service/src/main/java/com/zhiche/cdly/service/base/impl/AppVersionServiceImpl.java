package com.zhiche.cdly.service.base.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.cdly.domain.mapper.base.AppVersionMapper;
import com.zhiche.cdly.domain.model.base.AppVersion;
import com.zhiche.cdly.service.base.IAppVersionService;
import com.zhiche.cdly.service.utils.base.AppVersionTypeEnum;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author beidouxing
 * @since 2018-07-17
 */
@Service
public class AppVersionServiceImpl extends ServiceImpl<AppVersionMapper, AppVersion> implements IAppVersionService {

    @Override
    public AppVersion getLastVersionAndroid() {
        EntityWrapper<AppVersion> ew = new EntityWrapper<>();
        ew.eq("ext_1", AppVersionTypeEnum.ANDROID.getCode())
                .orderBy("gmt_create", false)
                .orderBy("id", false);
        Page<AppVersion> page = new Page<>(1, 1);
        List<AppVersion> appVersions = this.baseMapper.selectPage(page, ew);
        if (CollectionUtils.isNotEmpty(appVersions)) {
            return appVersions.get(0);
        }
        return null;
    }

    @Override
    public AppVersion getLastVersionIos() {
        EntityWrapper<AppVersion> ew = new EntityWrapper<>();
        ew.eq("ext_1", AppVersionTypeEnum.IOS.getCode())
                .orderBy("gmt_create", false)
                .orderBy("id", false);
        Page<AppVersion> page = new Page<>(1, 1);
        List<AppVersion> appVersions = this.baseMapper.selectPage(page, ew);
        if (CollectionUtils.isNotEmpty(appVersions)) {
            return appVersions.get(0);
        }
        return null;
    }
}
