package com.zhiche.cdly.service.base.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.cdly.domain.mapper.base.TestUserMapper;
import com.zhiche.cdly.domain.model.base.TestUser;
import com.zhiche.cdly.service.base.ITestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:28 2019/8/9
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestUserMapper, TestUser> implements ITestService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestServiceImpl.class);


    @Override
    public void queryTestData () {
        EntityWrapper<TestUser> ew = new EntityWrapper<>();
        ew.eq("name", "dd");
        List<TestUser> list = baseMapper.selectList(ew);
        LOGGER.info("返回数据为{}:", list);
    }
}
