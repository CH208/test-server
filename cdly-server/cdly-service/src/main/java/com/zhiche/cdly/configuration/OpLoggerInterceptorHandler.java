package com.zhiche.cdly.configuration;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * @author caihua
 * 日志打印
 */
@Aspect
@Component
@Order(3)   // 有多个日志时，ORDER可以定义切面的执行顺序（数字越大，前置越后执行，后置越前执行）
public class OpLoggerInterceptorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(OpLoggerInterceptorHandler.class);

    private static ThreadLocal<Long> startTime = new ThreadLocal<Long>();

    /**
     * 定义切入点
     * 1、execution 表达式主体
     * 2、第1个* 表示返回值类型  *表示所有类型
     * 3、包名  com.*.*.controller下
     * 4、第4个* 类名，com.*.*.controller包下所有类
     * 5、第5个* 方法名，com.*.*.controller包下所有类所有方法
     * 6、(..) 表示方法参数，..表示任何参数
     */
    @Pointcut("execution(public * com.zhiche..*.*Controller.*(..))")
    public void aopLog () {

    }

    @Before("aopLog()")
    public void doBefore (JoinPoint joinPoint) {
        //info ,debug ,warn ,erro四种级别，这里我们注入info级别
        startTime.set(System.currentTimeMillis());
        //获取servlet请求对象---因为这不是控制器，这里不能注入HttpServletRequest，但springMVC本身提供ServletRequestAttributes可以拿到
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String uri = request.getRequestURI();
        String ip = request.getRemoteAddr();
        String clazzName = joinPoint.getTarget().getClass().getName();
        LOGGER.info("LOGGER [INFO] ip:{}  ||controller:{}  ||uri:{}  ||params{}--[   start]", ip, clazzName, uri,
                Arrays.toString(joinPoint.getArgs()));
    }

    @After("aopLog()")
    public void doAfter (JoinPoint joinPoint) {
        //获取servlet请求对象---因为这不是控制器，这里不能注入HttpServletRequest，但springMVC本身提供ServletRequestAttributes可以拿到
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String uri = request.getRequestURI();
        String ip = request.getRemoteAddr();
        String clazzName = joinPoint.getTarget().getClass().getName();
        LOGGER.info("LOGGER [INFO]   ip:{} ||  controller:{} ||   method:{}  耗时:{}---[    end]", ip, clazzName, uri);
    }

    /**
     * 计算接口执行时间
     */
    @AfterReturning(pointcut = "aopLog()")
    public void doAfterReturing () {
        long costTime = System.currentTimeMillis() - startTime.get();
        LOGGER.info("=======> 耗费时间: " + costTime + "ms");
    }

}
