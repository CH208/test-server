package com.zhiche.cdly.app.vo;

import com.zhiche.cdly.domain.model.sys.User;

import java.io.Serializable;
import java.util.List;

/**
 * Created by beidouxing on 2018/6/23.
 */
public class UserVo extends User implements Serializable{

    private static final long serialVersionUID = 5727736792068473446L;

}
