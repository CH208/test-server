package com.zhiche.cdly.app.controller.base;

import com.zhiche.cdly.service.base.ITestService;
import com.zhiche.cdly.supports.response.RestfulResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 16:47 2019/8/9
 */
@RestController
public class TestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private ITestService iTestService;

    @RequestMapping("/hello")
    public RestfulResponse<String> queryTestData(){
        RestfulResponse<String> restfulResponse = new RestfulResponse<>(0,"success",null);
        iTestService.queryTestData();
        return restfulResponse;
    }
}
