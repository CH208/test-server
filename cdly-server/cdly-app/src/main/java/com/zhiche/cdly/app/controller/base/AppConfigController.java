package com.zhiche.cdly.app.controller.base;

import com.zhiche.cdly.domain.model.base.AppVersion;
import com.zhiche.cdly.dto.base.ResultDTOWithPagination;
import com.zhiche.cdly.service.base.IAppVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/version")
public class AppConfigController {

    @Autowired
    private IAppVersionService versionService;

    @RequestMapping("/android/latest")
    public ResultDTOWithPagination<AppVersion> updateAndroidApp() {
        AppVersion lastVersion = versionService.getLastVersionAndroid();
        return new ResultDTOWithPagination<>(true, lastVersion, 0, "查询成功", null);
    }

    @RequestMapping("/ios/latest")
    public ResultDTOWithPagination<AppVersion> updateIosApp() {
        AppVersion lastVersion = versionService.getLastVersionIos();
        return new ResultDTOWithPagination<>(true, lastVersion, 0, "查询成功", null);
    }

}
