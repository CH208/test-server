package com.zhiche.cdly.domain.model.sys;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户
 * </p>
 *
 * @author beidouxing
 * @since 2018-06-23
 */
@TableName("sys_user")
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 编码
     */
    private String code;
    /**
     * 姓名
     */
    private String name;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 电子邮箱
     */
    private String email;
    /**
     * 性别
     */
    private Integer gender;
    /**
     * 身份证号
     */
    @TableField("id_number")
    private String idNumber;
    /**
     * 出生日期
     */
    private Date birthday;
    /**
     * 所属部门
     */
    private String department;
    /**
     * 上级领导
     */
    private String leader;
    /**
     * 驾照类型(A1,A2,B1,B2,C)
     */
    @TableField("license_type")
    private String licenseType;
    /**
     * 驾照证号
     */
    @TableField("driving_license")
    private String drivingLicense;
    /**
     * 初次领证日期
     */
    @TableField("first_license_date")
    private Date firstLicenseDate;
    /**
     * 驾驶证有效期
     */
    @TableField("Invalid_license_date")
    private Date invalidLicenseDate;
    /**
     * 意外险有效期
     */
    @TableField("Invalid_insurance_date")
    private Date invalidInsuranceDate;
    /**
     * 账号ID
     */
    @TableField("account_id")
    private String accountId;
    /**
     * 状态(1:正常,0:失效)
     */
    private Integer status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    @TableField("create_user")
    private String createUser;
    /**
     * 修改人
     */
    @TableField("modified_user")
    private String modifiedUser;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;


    /**
     * 用户角色
     */
    @TableField(exist = false)
    private String roleName;

    /**
     * 发车点
     */
    @TableField(exist = false)
    private String deliveryCode;

    /**
     * 仓库
     */
    @TableField(exist = false)
    private String storeCode;

    public Integer getId () {
        return id;
    }

    public void setId (Integer id) {
        this.id = id;
    }

    public String getCode () {
        return code;
    }

    public void setCode (String code) {
        this.code = code;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getMobile () {
        return mobile;
    }

    public void setMobile (String mobile) {
        this.mobile = mobile;
    }

    public String getEmail () {
        return email;
    }

    public void setEmail (String email) {
        this.email = email;
    }

    public Integer getGender () {
        return gender;
    }

    public void setGender (Integer gender) {
        this.gender = gender;
    }

    public String getIdNumber () {
        return idNumber;
    }

    public void setIdNumber (String idNumber) {
        this.idNumber = idNumber;
    }

    public Date getBirthday () {
        return birthday;
    }

    public void setBirthday (Date birthday) {
        this.birthday = birthday;
    }

    public String getDepartment () {
        return department;
    }

    public void setDepartment (String department) {
        this.department = department;
    }

    public String getLeader () {
        return leader;
    }

    public void setLeader (String leader) {
        this.leader = leader;
    }

    public String getLicenseType () {
        return licenseType;
    }

    public void setLicenseType (String licenseType) {
        this.licenseType = licenseType;
    }

    public String getDrivingLicense () {
        return drivingLicense;
    }

    public void setDrivingLicense (String drivingLicense) {
        this.drivingLicense = drivingLicense;
    }

    public Date getFirstLicenseDate () {
        return firstLicenseDate;
    }

    public void setFirstLicenseDate (Date firstLicenseDate) {
        this.firstLicenseDate = firstLicenseDate;
    }

    public Date getInvalidLicenseDate () {
        return invalidLicenseDate;
    }

    public void setInvalidLicenseDate (Date InvalidLicenseDate) {
        this.invalidLicenseDate = InvalidLicenseDate;
    }

    public Date getInvalidInsuranceDate () {
        return invalidInsuranceDate;
    }

    public void setInvalidInsuranceDate (Date InvalidInsuranceDate) {
        this.invalidInsuranceDate = InvalidInsuranceDate;
    }

    public String getAccountId () {
        return accountId;
    }

    public void setAccountId (String accountId) {
        this.accountId = accountId;
    }

    public Integer getStatus () {
        return status;
    }

    public void setStatus (Integer status) {
        this.status = status;
    }

    public String getRemark () {
        return remark;
    }

    public void setRemark (String remark) {
        this.remark = remark;
    }

    public String getCreateUser () {
        return createUser;
    }

    public void setCreateUser (String createUser) {
        this.createUser = createUser;
    }

    public String getModifiedUser () {
        return modifiedUser;
    }

    public void setModifiedUser (String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    public Date getGmtCreate () {
        return gmtCreate;
    }

    public void setGmtCreate (Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified () {
        return gmtModified;
    }

    public void setGmtModified (Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal () {
        return this.id;
    }

    public String getRoleName () {
        return roleName;
    }

    public void setRoleName (String roleName) {
        this.roleName = roleName;
    }

    public String getDeliveryCode () {
        return deliveryCode;
    }

    public void setDeliveryCode (String deliveryCode) {
        this.deliveryCode = deliveryCode;
    }

    public String getStoreCode () {
        return storeCode;
    }

    public void setStoreCode (String storeCode) {
        this.storeCode = storeCode;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("id=").append(id);
        sb.append(", code='").append(code).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", mobile='").append(mobile).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", gender=").append(gender);
        sb.append(", idNumber='").append(idNumber).append('\'');
        sb.append(", birthday=").append(birthday);
        sb.append(", department='").append(department).append('\'');
        sb.append(", leader='").append(leader).append('\'');
        sb.append(", licenseType='").append(licenseType).append('\'');
        sb.append(", drivingLicense='").append(drivingLicense).append('\'');
        sb.append(", firstLicenseDate=").append(firstLicenseDate);
        sb.append(", invalidLicenseDate=").append(invalidLicenseDate);
        sb.append(", invalidInsuranceDate=").append(invalidInsuranceDate);
        sb.append(", accountId='").append(accountId).append('\'');
        sb.append(", status=").append(status);
        sb.append(", remark='").append(remark).append('\'');
        sb.append(", createUser='").append(createUser).append('\'');
        sb.append(", modifiedUser='").append(modifiedUser).append('\'');
        sb.append(", gmtCreate=").append(gmtCreate);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", roleName='").append(roleName).append('\'');
        sb.append(", deliveryCode='").append(deliveryCode).append('\'');
        sb.append(", storeCode='").append(storeCode).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
