package com.zhiche.cdly.domain.mapper.base;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.cdly.domain.model.base.AppVersion;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author beidouxing
 * @since 2018-07-17
 */
public interface AppVersionMapper extends BaseMapper<AppVersion> {

}
