package com.zhiche.cdly.dto.sys;

import com.zhiche.cdly.domain.model.sys.User;

import java.io.Serializable;

public class UserDTO extends User implements Serializable {

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
