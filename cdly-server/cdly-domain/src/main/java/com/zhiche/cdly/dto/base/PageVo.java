package com.zhiche.cdly.dto.base;

import java.io.Serializable;

/**
 * 分页
 */
public class PageVo implements Serializable {

    private static final long serialVersionUID = 4289171840045697554L;
    private int pageNo = 1;
    private int pageSize = 10;
    private int totalRecord = 0;
    private int totalPage = 1;
    private String order = "";

    public int getPageNo() {
        return this.pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalRecord() {
        return this.totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public int getTotalPage() {
        this.totalPage = this.totalRecord % this.pageSize == 0 ? this.totalRecord / this.pageSize : this.totalRecord / this.pageSize + 1;
        this.setTotalPage(this.totalPage);
        return this.totalPage;
    }

    private void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public String getOrder() {
        return this.order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public int getStartIndex() {
        return (this.pageNo - 1) * this.pageSize;
    }

    public String toString() {
        return "PageVo [pageNo=" + this.pageNo + ", pageSize=" + this.pageSize + ", totalRecord=" + this.totalRecord + ", totalPage=" + this.totalPage + ", order=" + this.order + "]";
    }
}
