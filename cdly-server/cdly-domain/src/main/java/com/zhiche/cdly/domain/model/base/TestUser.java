package com.zhiche.cdly.domain.model.base;

import com.baomidou.mybatisplus.annotations.TableName;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:17 2019/8/9
 */
@TableName("test_user")
public class TestUser {
    private long id;

    private String name;

    private String age;

    public long getId () {
        return id;
    }

    public void setId (long id) {
        this.id = id;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getAge () {
        return age;
    }

    public void setAge (String age) {
        this.age = age;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("TestUser{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", age='").append(age).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
