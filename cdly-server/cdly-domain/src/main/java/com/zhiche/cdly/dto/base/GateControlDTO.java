package com.zhiche.cdly.dto.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "闸门控制传输对象")
public class GateControlDTO implements Serializable {

    /**
     * 通道编号
     */
    @ApiModelProperty(notes = "闸门通道号/大门区003,004;库存区001,002")
    private String channel;
    /**
     * 车牌号
     */
    @ApiModelProperty(notes = "车牌号")
    private String platenum;
    /**
     * 出发时间
     */
    @ApiModelProperty(notes = "出发时间", allowEmptyValue = true)
    private String ymdhms;
    /**
     * 卡种类
     */
    @ApiModelProperty(notes = "卡种类", allowEmptyValue = true)
    private String kind;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getPlatenum() {
        return platenum;
    }

    public void setPlatenum(String platenum) {
        this.platenum = platenum;
    }

    public String getYmdhms() {
        return ymdhms;
    }

    public void setYmdhms(String ymdhms) {
        this.ymdhms = ymdhms;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }
}
