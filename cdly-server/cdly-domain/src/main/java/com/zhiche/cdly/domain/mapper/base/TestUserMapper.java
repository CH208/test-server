package com.zhiche.cdly.domain.mapper.base;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.cdly.domain.model.base.TestUser;

/**
 * @Author: caiHua
 * @Description: 测试类mapper接口
 * @Date: Create in 17:20 2019/8/9
 */
public interface TestUserMapper extends BaseMapper<TestUser> {
}
