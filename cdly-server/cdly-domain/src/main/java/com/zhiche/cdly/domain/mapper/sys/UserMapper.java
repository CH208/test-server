package com.zhiche.cdly.domain.mapper.sys;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.cdly.domain.model.sys.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author beidouxing
 * @since 2018-06-23
 */
public interface UserMapper extends BaseMapper<User> {

    /**
     * 用户列表查询
     */
    List<User> selectUserPage (Page<User> page, @Param("ew") EntityWrapper<User> ew);
}
